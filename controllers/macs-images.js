var Mac = require('../models/mac-address')
var Image = require('../models/image')
var matchController = require('../controllers/match-controller')
exports.addRelationship = function(macsList, imagesList) {
    var macsId = []
    var imagesId = []
    for (i = 0; i < macsList.length; i++) {
        macsId.push(macsList[i]._id)
    }
    for (i = 0; i < imagesList.length; i++) {
        imagesId.push(imagesList[i]._id)
    }
    for (i = 0; i < macsList.length; i++) {
        macsList[i].images = imagesId
        macsList[i].save()
    }
    for (i = 0; i < imagesList.length; i++) {
        imagesList[i].macs = macsId
        imagesList[i].save()
    }

    // put info into match table
    for (i = 0; i < macsId.length; i++) {
        for (j = 0; j < imagesId.length; j++) {
            matchController.saveMatch(0, macsId[i], imagesId[j], (err, entry) => {
                if (err)
                    console.log(err);
                // console.log(entry);
            })
        }
    }
}
