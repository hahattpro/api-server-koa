var fs = require('fs')
var request = require('request')
var Image = require('../models/image')
var apiKey = require('../const/api-key')

exports.uploadImgur = function(filepath, callback) {
    console.log('FILEPATH ', filepath)
    var formData = {
        image: {
            value: fs.createReadStream(filepath),
            options: {
                filename: 'meow',
                contentType: 'image/jpg'
            }
        }
    }

    var options = {
        url: 'https://api.imgur.com/3/upload',
        headers: {
            'Authorization': 'Client-ID ' + apiKey.imgurClientID1
        },
        formData: formData
    }

    request.post(options, function optionalCallback(err, httpResponse, body) {
        if (err) {
            console.error('upload failed:', err)
            callback(err, null)
        }
        if (httpResponse.statusCode !== 200) {
            console.error(httpResponse.statusCode);
            return callback('upload failed: status not 200', null)
        }
        body = JSON.parse(body)
        var link = body.data.link
        callback(null, link)
    })
}

// save a imgur link into database
exports.saveLink = function(link, latlngId, callback) {
    var imageEntry = new Image({
        link: link
    })
    if (latlngId != null) {
        imageEntry.latlng = latlngId
    }
    imageEntry.save((err, entry) => {
        callback(err, entry)
    })
}

exports.addLatlng = function(entry, latlngId, callback) {
    entry.latlng = latlngId
    entry.save((err, m) => {
        if (callback != null) {
            callback(err, m)
        }
    })
}

// retrieve all image database entry
exports.findallImage = function(callback) {
    Image.find((err, m) => {
        callback(err, m)
    })
}
