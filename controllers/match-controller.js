var Match = require('../models/match')

exports.saveMatch = function(percent, macAddress, imageId, callback) {
    var matchEntry = new Match({
        percent: percent,
        mac: macAddress,
        imageId: imageId
    })
    matchEntry.save((err, entry) => {
        callback(err, entry)
    })

}

exports.removeMatchByMac = function(macAddress) {
    Match.remove({ mac: macAddress }, (err) => {
        callback(err, macAddress)
    })
}

exports.removeMatchByImage = function(imageId) {
    Match.remove({ imageId: imageId }, (err) => {
        callback(err, imageId)
    })
}

exports.findByMac = function(macAddress) {
    Match.findOne({ mac: macAddress }, 'mac image percent', function(err, Match) {
        if (err)
            return handleError(err)
        console.log('%s match with %s by %s percent', Match.mac, Match.imageId, Match.percent)
    })
}

exports.findByImage = function(imageId) {
    Match.findOne({ imageId: imageId }, 'mac image percent', function(err, Match) {
        if (err)
            return handleError(err)
        console.log('%s match with %s by %s percent', Match.mac, Match.imageId, Match.percent)
    })
}
