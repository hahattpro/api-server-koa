var Mac = require('../models/mac-address')

exports.saveMac = function(macAddress, latlngId, callback) {
    oldmac = Mac.findOne({'mac':macAddress}, function(err, m){
        // find if old macs exist
        if (err) {
            console.log(err)
            return
        }
        if (m == null){
            latlngempty = []
            m = new Mac({// create new entry if it is new
                mac: macAddress,
                latlng: latlngempty
            })
        }
        if (latlngId != null) {
            m.latlng.push(latlngId)
        }
        m.save((err, m) => {
            callback(err, m)
        })
    })


}

exports.removeMac = function(macAddress, callback) {
    Mac.remove({ mac: macAddress }, (err) => {
        callback(err, macAddress)
    })
}

// given a mac address
// find a mac object
exports.findMacByAddress = function(macAddress, callback){
    Mac.findOne({'mac':macAddress}) // findOne because each Mac Address is unique
        .deepPopulate('images latlng') // populate ref images and latlng
        .exec(function(err, mac){
            callback(err, mac)
        })
}

// this is a trunk
// an asynchronous function that takes only one argument: a callback with the prototype (err, res)
exports.findallMac = function(callback) {
    Mac.find((err, m) => {
        callback(err, m)
    })
}
