var dbURI = process.env.MONGODB_URI || 'mongodb://localhost/api-server-koa';
var should = require('chai').should();
var mongoose = require('mongoose');

var Match = require('../models/match')
var Mac = require('../models/mac-address')
var Image = require('../models/image')

var clearDB = require('mocha-mongoose')(dbURI, { skip: ['images', 'macs', 'matches'] });

describe("Test Match Controler", function() {
    beforeEach(function(done) {
        if (mongoose.connection.db)
            return done();
        mongoose.connect(dbURI, done);

    });
    var testMac = new Mac({
        mac: "AA-BB-CC-DD-EE-FF"
    }).save();
    var testImage = new Image({
        _id: 123,
        link: "http://i.imgur.com/TLAda1P.png"
    }).save();

    it('can save Match', function(done) {

        new Match({
            percent: 80,
            mac: "AA-BB-CC-DD-EE-FF",
            imageId: 123
        }).save(done);
    })

    it('can find one by mac', function(done) {
        Match.findOne({ mac: "AA-BB-CC-DD-EE-FF" }, function(err, result) {
            if (err)
                return done(err);
            result.imageId.should.equal(123);
            done();
        })
    })

    it('can find one by image link', function(done) {
        Match.findOne({ imageId: 123 }, function(err, result) {
            if (err)
                return done(err);
            result.mac.should.equal("AA-BB-CC-DD-EE-FF");
            done();
        })
    })
});
