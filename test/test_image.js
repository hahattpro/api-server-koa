var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../index.js')
var should = chai.should()
var fs = require('fs')

chai.use(chaiHttp)

describe('Send image', function() {
    it('it should take upload image by POST method', function(done) {
        this.timeout(10000)
        chai.request(server)
            .post('/api/image')
            .attach('image', fs.readFileSync('testimg.png'), 'testimg.png')
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })
})
