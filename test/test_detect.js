var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../index.js')
var should = chai.should()
var fs = require('fs')
var Mac = require('../models/mac-address')

chai.use(chaiHttp)

describe('Send image', function() {


    it('it should take multiple image and macs using POST', function(done) {
        this.timeout(20000)
        chai.request(server)
            .post('/api/detect')
            .attach('image', fs.readFileSync('testimg.png'), 'testimg.png')
            .attach('image1', fs.readFileSync('testimg1.jpg'), 'testimg1.jpg')
            .attach('image2', fs.readFileSync('testimg2.jpg'), 'testimg2.jpg')
            .field('data', '{"latlng": {"lat": 1.0,"lng": 123},"macs": [{"mac": "AA-BB-CC-MM-EE-FF"}, {"mac": "AA-BB-44-33-22-11"}, {"mac": "AA-ME-OW-DD-EE-FF"}]}')
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    it('mac AA-BB-CC-MM-EE-FF', function(done) {
        macAddress = 'AA-BB-CC-MM-EE-FF'
        Mac.findOne({ mac: macAddress }, function(err, result) {
            if (err)
                return done(err);
            result.should.have.property('images')
            result.mac.should.equal(macAddress)
            Mac.remove({ mac: macAddress }, function(err, result){
                if (err)
                    return done(err)
                done();
            })
        })
    })
    it('mac AA-BB-44-33-22-11', function(done) {
        macAddress = 'AA-BB-44-33-22-11'
        Mac.findOne({ mac: macAddress }, function(err, result) {
            if (err)
                return done(err);
            result.should.have.property('images')
            result.mac.should.equal(macAddress)
            Mac.remove({ mac: macAddress }, function(err, result){
                if (err)
                    return done(err)
                done();
            })
        })
    })
    it('mac AA-ME-OW-DD-EE-FF', function(done) {
        macAddress = 'AA-ME-OW-DD-EE-FF'
        Mac.findOne({ mac: macAddress }, function(err, result) {
            if (err)
                return done(err);
            result.should.have.property('images')
            result.mac.should.equal(macAddress)
            Mac.remove({ mac: macAddress }, function(err, result){
                if (err)
                    return done(err)
                done();
            })
        })
    })
})
