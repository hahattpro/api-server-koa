var dbURI = process.env.MONGODB_URI || 'mongodb://localhost/api-server-koa';
var should = require('chai').should();
var mongoose = require('mongoose');
var Dummy = mongoose.model('Dummy', new mongoose.Schema({ a: Number }));
var clearDB = require('mocha-mongoose')(dbURI);

describe("Basic mongoose functions", function() {
    beforeEach(function(done) {
        if (mongoose.connection.db)
            return done();

        mongoose.connect(dbURI, done);
    });

    it('can save', function(done) {
        new Dummy({ a: 1 }).save(done);
    });

    it("can be listed", function(done) {
        new Dummy({ a: 1 }).save(function(err, model) {
            if (err) return done(err);

            new Dummy({ a: 2 }).save(function(err, model) {
                if (err) return done(err);

                Dummy.find({}, function(err, docs) {
                    if (err) return done(err);

                    // without clearing the DB between specs, this would be 3
                    docs.length.should.equal(2);
                    done();
                });
            });
        });
    });

    it('can clear DB', function(done) {
        new Dummy({ a: 5 }).save(function(err, model) {
            if (err) return done(err);

            clearDB(function(err) {
                if (err) return done(err);

                Dummy.find({}, function(err, docs) {
                    if (err) return done(err);

                    docs.length.should.equal(0);
                    done();
                });
            });
        });
    });

});
