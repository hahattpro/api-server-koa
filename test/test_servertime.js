var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../index.js')
var should = chai.should()

chai.use(chaiHttp)

describe('Server time', () => {
    it('it should GET time in ISO-8601 standard and milliseconds', (done) => {
        chai.request(server)
            .get('/api/time')
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.have.property('standard')
                res.body.should.have.property('milliseconds')
                done()
            })
    })
})
