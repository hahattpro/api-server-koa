var detector = require('koa-router')()
var parse = require('co-busboy')
var fs = require('fs')
var path = require('path')
var temp = require('temp')
var uuid = require('node-uuid')

var admin = require("firebase-admin");
var database = admin.database()


// var config = {
//     apiKey: "AIzaSyBNeJcW6CKp9v7z9HjRJHcOj7Sw7qY14AM",
//     authDomain: "api-server-koa.firebaseapp.com",
//     databaseURL: "https://api-server-koa.firebaseio.com",
//     storageBucket: "api-server-koa.appspot.com",
//     messagingSenderId: "409014105383"
// };
// admin.initializeApp(config);

var imageController = require('../controllers/image-controller')
var macController = require('../controllers/mac-controller')
var macsImages = require('../controllers/macs-images')
var adsController = require('../controllers/ads-controller')
var latlngController = require('../controllers/latlng-controller')

temp.track()

var data

var counter = 0

// var imageNotificationFirebaseRef = admin.database().ref('rasId');

// imageNotificationFirebaseRef.on('value', function() {})


detector.post('/api/detect', function*(next) {
    var parts = parse(this, {
        autoFields: true
    })
    var part

    var links = []
    var imagesList = []
    while ((part = yield parts)) {
        var imageObj = yield saveImageReturnImageObj(part)
        if (imageObj !== 'error') {
            imagesList.push(imageObj)
            links.push(imageObj.link)
        }
    }

    data = JSON.parse(parts.fields[0][1])
    var latlngEntry = yield saveLatlngReturnLatlngEntry()

    var macs = data.macs
    if (macs == null) {
        macs = [];
    }
    var macsList = []
    for (i = 0; i < macs.length; i++) {
        var entry = yield saveMacWithLatlngEntryReturnMacEntry(macs, latlngEntry)
        macsList.push(entry)
    }

    var macArray = []
    for (i = 0; i < macsList.length; i++) {
        macArray.push(macsList[i].mac)
    }

    // console.log(links)
    // console.log(macArray)

    var id = uuid.v4();
    database.ref('upload/' + data.idDevice).update({
        frameId: id,
        macs: macArray,
        links: links
    })

    for (i = 0; i < imagesList.length; i++) {
        var entry = yield addLatlngEntryToImageListReturnImageEntry(latlngEntry, imagesList)
        imagesList[i] = entry
    }

    macsImages.addRelationship(macsList, imagesList)

    // var ads = yield adsController.getAds
    this.response.status = 200
    this.response.body = id
})

module.exports = detector

function saveImageReturnImageObj(part) {
    var id = uuid.v4()
    fs.mkdirSync('images/' + id)
    var link = 'images/' + id + '/' + part.fieldname + ".jpg";
    var stream = fs.createWriteStream(link);
    return new Promise((resolve, reject) => {
        part.pipe(stream).on('close', () => {
            saveImageWithLink(stream.path, resolve)
        })
    })
}

// function saveImage(stream, resolve) {
//     imageController.uploadImgur(stream.path, (err, link) => {
//         if (err) {
//             this.response.status = 500
//             this.response.body = 'Internal Error Server'
//             console.log('upload image error')
//             return resolve('error')
//         }
//         saveImageWithLink(link, resolve)
//     })
// }

function saveImageWithLink(link, resolve) {
    imageController.saveLink(link, null, (err, entry) => {
        if (err) {
            return resolve('error')
        }
        resolve(entry)
    })
}

function saveLatlngReturnLatlngEntry() {
    var latlng = data.latlng
    return new Promise((resolve, reject) => {
        latlngController.saveLatlng(latlng, (err, entry) => {
            if (err) {
                console.log(err);
                resolve()
            } else {
                resolve(entry)
            }
        })
    })
}

function saveMacWithLatlngEntryReturnMacEntry(macs, latlngEntry) {
    return new Promise((resolve, reject) => {
        macController.saveMac(macs[i].mac, latlngEntry._id, (err, m) => {
            resolve(m)
        })
    })
}

function addLatlngEntryToImageListReturnImageEntry(latlngEntry, imagesList) {
    return new Promise((resolve, reject) => {
        imageController.addLatlng(imagesList[i], latlngEntry._id, (err, entry) => {
            resolve(entry)
        })
    })
}
