var resultCollector = require('koa-router')();
var bodyParser = require('koa-bodyparser');

var admin = require("firebase-admin");
var database = admin.database();

resultCollector.use(bodyParser());

resultCollector.post('/api/result', function*(next) {
    var data = this.request.body;
    var macs = data.macs;
    var id = data.id;

    database.ref('result/' + id).update({
        macs: macs
    })

    this.response.status = 200;
    this.response.type = 'application/json'
})

module.exports = resultCollector;
