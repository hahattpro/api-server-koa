var imageCollector = require('koa-router')()
var parse = require('co-busboy')
var fs = require('fs')
var path = require('path')
var temp = require('temp')
var imageController = require('../controllers/image-controller')

temp.track()

imageCollector.get('/api/image', function*(next) {
    this.response.status = 200
    this.response.type = 'application/json'
    this.response.body = yield imageController.findallImage
})

module.exports = imageCollector
