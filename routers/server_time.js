var bodyParser = require('koa-bodyparser')
var server_time = require('koa-router')()

server_time.use(bodyParser())

/*
GET method
res.body.standard = ISO-8601 standard
res.body.milliseconds = number of milliseconds since 1970/01/01
*/
server_time.get('/api/time', function*(next) {
    var d = new Date()
    var output_date = {}
        //JSON dates have the same format as the ISO-8601 standard
    output_date.standard = d.toJSON()
        // Return the number of milliseconds since 1970/01/01:
    output_date.milliseconds = d.getTime()
    this.response.status = 200
    this.response.body = output_date

})

module.exports = server_time
