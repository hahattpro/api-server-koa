var bodyParser = require('koa-bodyparser')
var macCollector = require('koa-router')()
var macController = require('../controllers/mac-controller')

macCollector.use(bodyParser())

// return all mac address
macCollector.get('/api/mac', function*(next) {
    // http://stackoverflow.com/questions/22037287/can-not-set-header-in-koa-when-using-callback
    // http://stackoverflow.com/questions/22059775/using-callbacks-with-nodejs-in-koa
    this.response.status = 200
    this.response.type = 'application/json'
    this.response.body = yield macController.findallMac

})
macCollector.post('/api/findMacByAddress', function*(next) {
    this.response.status = 200;
    this.response.type = 'application/json'
    var macAddress = this.request.body.mac;
    this.response.body = yield new Promise((resolve, reject) => {
        macController.findMacByAddress(macAddress, (err, m) => {
            resolve(m)
        })
    })
})
module.exports = macCollector
