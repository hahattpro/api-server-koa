var mongoose = require('mongoose')
var MatchSchema = mongoose.Schema({
    percent: {
        type: Number,
        require: true
    },
    mac: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Mac'
    },
    imageId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Image'
    }
})

var Match = mongoose.model('Match', MatchSchema)

module.exports = Match
