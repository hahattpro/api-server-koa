var mongoose = require('mongoose')
var Latlng = require('./latlng')
var Mac = require('./mac-address')
var deepPopulate = require('mongoose-deep-populate')(mongoose)
// Schema for table contain Image info
var ImageSchema = mongoose.Schema({
    link: {
        type: String,
        required: true
    },
    macs: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Mac'
    }],
    latlng: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Latlng'
    }]

})

ImageSchema.plugin(deepPopulate, {
  whitelist: [
    'macs',
    'latlng',
    'macs.latlng'
  ]
});

// mac table
var Image = mongoose.model('Image', ImageSchema)

module.exports = Image
