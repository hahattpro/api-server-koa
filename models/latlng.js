var mongoose = require('mongoose')


// Schema for table contain coord
var LatlngSchema = mongoose.Schema({
    lat: { type: Number, require: true },
    lng: { type: Number, require: true },
    timestamp: { type: Date, default: Date.now }
})

var Latlng = mongoose.model('Latlng', LatlngSchema)

module.exports = Latlng
