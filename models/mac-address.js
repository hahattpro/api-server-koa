var mongoose = require('mongoose')
mongoose.Promise = global.Promise
var deepPopulate = require('mongoose-deep-populate')(mongoose)
// Schema for table contain mac address
var MacSchema = mongoose.Schema({
    mac: {
        type: String,
        required: true
    },
    images: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Image'
    }],
    latlng: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Latlng'
    }]
})

MacSchema.plugin(deepPopulate, {
  whitelist: [
    'latlng',
    'images'
  ]
});

// mac table
var Mac = mongoose.model('Mac', MacSchema)

Mac.test = function() {
    console.log('meow')
}

module.exports = Mac
