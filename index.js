var koa = require('koa')
var mongoose = require('mongoose')
var serve = require('koa-static')

var admin = require("firebase-admin");
var serviceAccount = require("./firebase/api-server-koa-firebase-adminsdk-mdpov-6116bae0cf.json");
var defaultApp = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://api-server-koa.firebaseio.com"
});

var macCollector = require('./routers/mac-collector')
var imageCollector = require('./routers/image-collector')
var server_time = require('./routers/server_time')
var detector = require('./routers/detector')
var resultCollector = require('./routers/result-collector')

mongoose.Promise = global.Promise // set global promise for mongoose
    // do this once for all
var mongodbUri = process.env.MONGODB_URI || 'mongodb://localhost/api-server-koa'
var port = process.env.PORT || 3000
mongoose.connect(mongodbUri)



var app = module.exports = koa()
app.use(macCollector.routes())
app.use(imageCollector.routes())
app.use(server_time.routes())
app.use(detector.routes())
app.use(resultCollector.routes())
app.use(serve('./'))

var server = app.listen(port, () => {
    console.log('server up and running')
    console.log('Listening on port %d', port);
})

module.exports = server
