Default port: 3000
## How to run
1. [Install nodejs](https://nodejs.org/en/)
2. [Install mongodb](https://docs.mongodb.com/v3.2/installation/)
    * [Windows](https://docs.mongodb.com/v3.2/tutorial/install-mongodb-on-windows/)
    * [OS X](https://docs.mongodb.com/v3.2/tutorial/install-mongodb-on-os-x/)
    * [Linux](https://docs.mongodb.com/v3.2/administration/install-on-linux/)
3. Install all dependency:
```
npm install
```
4. Run mongodb
5. Start server
  ```
  npm start
  ```

## Api Usage
### MAC Collector:
hostname:port/api/mac <br>
(example: localhost:3000/api/mac)

Method: POST    <br>
Request:<br>
Content-Type: application/json <br>
Body: <br>
```javascript
{
    "mac":"AA-BB-CC-DD-EE-FF"
}
```
Response: (as text/plain)<br>
received AA-BB-CC-DD-EE-FF<br>

Method: GET <br>
It return all Mac address and timestamp as json <br>
Response: <br>
Status 200
Content-Type: application/json <br>
Body: an array of Mac address entries as json object

**Example**
```javascript
[
  {
    "_id": "58369d3061cc33283ceba2b1",
    "mac": "AA-BB-CC-DD-EE-22",
    "__v": 0,
    "timestamp": "2016-11-24T07:56:32.720Z"
  },
  {
    "_id": "58369d3961cc33283ceba2b2",
    "mac": "AA-BB-CC-DD-EE-22",
    "__v": 0,
    "timestamp": "2016-11-24T07:56:41.689Z"
  },
  {
    "_id": "5836a211d540b4099cde1d81",
    "mac": "AA-BB-CC-DD-EE-22",
    "__v": 0,
    "timestamp": "2016-11-24T08:17:21.301Z"
  },
  {
    "_id": "5836a224d540b4099cde1d82",
    "mac": "11-22-33-44-55-66-77-88",
    "__v": 0,
    "timestamp": "2016-11-24T08:17:40.683Z"
  }
]
```

Method: GET<br>
Body: all database entries as json

```javascript
[
  {
    "_id": "583867fe80d01bf6fc4e8cc2",
    "mac": "AA-BB-CC-DD-EE-FF",
    "__v": 0,
    "timestamp": "2016-11-25T16:34:06.867Z"
  },
  {
    "_id": "5838680780d01bf6fc4e8cc3",
    "mac": "AA-ME-OW-DD-EE-FF",
    "__v": 0,
    "timestamp": "2016-11-25T16:34:15.960Z"
  },
  {
    "_id": "5838681380d01bf6fc4e8cc4",
    "mac": "AA-ME-OW-ME-OW-FF",
    "__v": 0,
    "timestamp": "2016-11-25T16:34:27.015Z"
  }
]
```

### Image Collector:<br>
hostname:port/api/image<br>
(example: localhost:3000/api/image)

Method: POST<br>
Request: (as form-data)
Content-Type: multipart/form-data not working
in postman, omit Content-Type: multipart/form-data header make it work
(bug ? perharp ?)
Postman request add Content-Type header like this
Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryJ5t9HvP5fyTQ6lMc


Parameter:<br>
"image": your file<br>

Response:<br>
Status: 200 <br>
Content-Type: text/plain <br>
Body: link to imgur

Method: GET<br>
body: all database entry as json
```javascript
[
  {
    "_id": "5838686680d01bf6fc4e8cc5",
    "link": "http://i.imgur.com/q0YJoJD.jpg",
    "__v": 0,
    "timestamp": "2016-11-25T16:35:50.947Z"
  },
  {
    "_id": "5838687280d01bf6fc4e8cc6",
    "link": "http://i.imgur.com/MJuWxwF.jpg",
    "__v": 0,
    "timestamp": "2016-11-25T16:36:02.175Z"
  },
  {
    "_id": "5838687b80d01bf6fc4e8cc7",
    "link": "http://i.imgur.com/JJXrVAP.jpg",
    "__v": 0,
    "timestamp": "2016-11-25T16:36:11.708Z"
  }
]
```

### Time
api/time  ( Raspberry team request this feature)
Method:  GET
response
```javascript
{
  "standard": "2016-12-01T17:59:24.350Z",
  "milliseconds": 1480615164350
}
```
With:
Standard is  ISO-8601 standard
Milliseconds is  number of milliseconds since 1970/01/01:

### Detect
api/detect

Method: POST
Request: (as form-data)
Content-Type: multipart/form-data not working
in postman, omit Content-Type: multipart/form-data header make it work
(bug ? perharp ?)


This api take input multiple images or macs and return ads info as JSON ( ads info is just mock data)


For image: key can be anything, as long as the value is png/jpg file.
Can take multiple key-images

For data
Key is Data:
Value is mac and coordinate latlng in JSON array  
{"latlng": {"lat": 1.0,"lng": 123},"macs": [{"mac": "AA-BB"}, {"mac": "AA-CC"}, {"mac": "AA-DD"}]}

Response:
Content-Type: application/json
```javascript
{  "id": "MEOW123",
  "timestamp": "2016-12-01T18:13:24.768Z"}
```
![sample]('./apidetectexample.JPG')
